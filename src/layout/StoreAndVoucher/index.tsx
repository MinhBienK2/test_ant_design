import Layout, { Header } from 'antd/lib/layout/layout';
import React from 'react';
import HeaderC from 'app/components/HeaderC';
import Banner from './components/Banner';
import { Divider, Tabs } from 'antd';

export default function index(props: any) {
  return (
    <Layout className="parentLess_detail">
      <HeaderC></HeaderC>
      <Layout
        style={{
          maxWidth: '1440px',
          width: '100%',
          margin: '0 auto',
          alignItems: 'center',
        }}
      >
        <Layout
          style={{
            maxWidth: '1170px',
            width: '100%',
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <Banner></Banner>
          <Divider></Divider>
          <Layout
            style={{
              alignItems: 'center',
            }}
          >
            <Tabs
              defaultActiveKey="1"
              items={[
                {
                  label: `Tab 1`,
                  key: '1',
                  children: props.children,
                },
                {
                  label: `Tab 2`,
                  key: '2',
                  children: 'hello2',
                },
                {
                  label: `Tab 3`,
                  key: '3',
                  children: 'hello 3',
                },
                {
                  label: `Tab 4`,
                  key: '4',
                  children: 'tab 4',
                },
                {
                  label: `Tab 5`,
                  key: '5',
                  children: 'tab 5',
                },
                {
                  label: `Tab 6`,
                  key: '6',
                  children: 'tab 6',
                },
                {
                  label: `Tab 7`,
                  key: '7',
                  children: 'tab 7',
                },
              ]}
            />
          </Layout>
        </Layout>
      </Layout>
    </Layout>
  );
}

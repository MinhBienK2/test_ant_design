import Layout, { Header } from 'antd/lib/layout/layout';
import React from 'react';
import HeaderC from 'app/components/HeaderC';
import Banner from './components/Banner';
import Body from './components/Body';
import { Divider } from 'antd';

export default function index() {
  return (
    <Layout className="parentLess_detail">
      <HeaderC></HeaderC>
      <Layout
        style={{
          maxWidth: '1440px',
          width: '100%',
          margin: '0 auto',
          alignItems: 'center',
        }}
      >
        <Layout
          style={{
            maxWidth: '1170px',
            width: '100%',
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <Banner></Banner>
          <Divider></Divider>
          <Body></Body>
        </Layout>
      </Layout>
    </Layout>
  );
}

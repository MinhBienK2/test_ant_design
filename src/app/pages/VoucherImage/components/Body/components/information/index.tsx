import { Col, Row } from 'antd';
import React from 'react';
import styled from 'styled-components';

export default function index() {
  return (
    <Container>
      <Box style={{ height: '115px', background: '#fffff', margin: '0 auto' }}>
        <div
          style={{
            display: 'grid',
            gridTemplateColumns: 'repeat(auto-fill, 200px)',
            gap: '17px',
            justifyContent: 'center',
            width: '100%',
          }}
        >
          <Image></Image>
          <Image></Image>
          <Image></Image>
          <Image></Image>
          <Image></Image>
          <Image></Image>
          <Image></Image>
          <Image></Image>
          <Image></Image>
          <Image></Image>
        </div>
      </Box>
      <br />
      <Box style={{ height: '115px', background: '#fffff' }}></Box>
    </Container>
  );
}

const Container = styled.div`
  // width: 100%;
  // max-width: 1030px;
  // margin: 0 auto;
`;

const Box = styled.div`
  padding: 0 30px;
  display: flex;
  justify-content: 'content';
`;

const Image = styled.div`
  width: 200px;
  height: 200px;
  background: red;
`;

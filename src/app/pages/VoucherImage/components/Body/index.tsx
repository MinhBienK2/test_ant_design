import { Layout, Tabs } from 'antd';
import React from 'react';
import '../../index.less';
import Information from './components/information';

export default function index() {
  return (
    <Layout
      style={{
        alignItems: 'center',
      }}
    >
      <Tabs
        defaultActiveKey="1"
        items={[
          {
            label: `Tab 1`,
            key: '1',
            children: <Information />,
          },
          {
            label: `Tab 2`,
            key: '2',
            children: 'hello2',
          },
          {
            label: `Tab 3`,
            key: '3',
            children: 'hello 3',
          },
          {
            label: `Tab 4`,
            key: '4',
            children: 'tab 4',
          },
          {
            label: `Tab 5`,
            key: '5',
            children: 'tab 5',
          },
          {
            label: `Tab 6`,
            key: '6',
            children: 'tab 6',
          },
          {
            label: `Tab 7`,
            key: '7',
            children: 'tab 7',
          },
        ]}
      />
    </Layout>
  );
}

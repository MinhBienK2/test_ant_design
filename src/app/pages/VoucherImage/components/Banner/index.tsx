import { Button, Col, Layout, Row } from 'antd';
import React from 'react';
import styled from 'styled-components';

export default function index() {
  return (
    <Layout>
      <Banner></Banner>
      <Row justify={'space-between'} wrap={false}>
        <Col
          span={'23.0769230769%'}
          style={{
            maxWidth: '270px',
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          <Logo></Logo>
        </Col>
        <Col
          span={'76.9230769231%'}
          style={{ maxWidth: '900px', width: '100%' }}
        >
          <Title>BHD Cinema - Combo 129k cho 2 vé xem phim và 1 bỏng</Title>
          <Row justify={'space-between'}>
            <Col>
              <p>Hạn dùng: 11/11/2022</p>
            </Col>
            <Col>
              <div>
                <Button>Đăng bán</Button>
                <Button>Dùng ngay</Button>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </Layout>
  );
}

const Banner = styled.div`
  max-width: 1170px;
  width: 100%;
  height: 358px;
  background: beige;
`;

const Title = styled.h1``;

const Logo = styled.div`
  width: 170px;
  height: 170px;
  background-color: red;
`;

import { Tabs } from 'antd';
import React from 'react';
import styled from 'styled-components';
import SearchS from './components/Search/Search';
import './style.less';
import TagContainVoucher from './components/TagContainVoucher';

export default function ContentSelect() {
  return (
    <ContainerS className="parent_content">
      <SearchS></SearchS>
      <VoucherItem>
        <Tabs
          defaultActiveKey="1"
          // centered={true}
          // tabBarGutter={320}
          className="parentTabs"
          items={[
            {
              label: `Tab 1`,
              key: '1',
              children: <TagContainVoucher color={'red'} />,
            },
            {
              label: `Tab 2`,
              key: '2',
              children: <TagContainVoucher color={'blue'} />,
            },
            {
              label: `Tab 3`,
              key: '3',
              children: <TagContainVoucher color={'black'} />,
            },
          ]}
        />
      </VoucherItem>
    </ContainerS>
  );
}

const ContainerS = styled.div`
  width: 100%;
  max-width: 764px;
  margin: auto;
`;

const VoucherItem = styled.div``;

import React from 'react';

export default function ItemVoucher(props: { color: string }) {
  return (
    <div
      style={{ width: '370px', height: '159px', background: `${props.color}` }}
    >
      ItemVoucher
    </div>
  );
}

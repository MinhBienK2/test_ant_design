import React from 'react';
import { Input } from 'antd';
import LogoI from '../../assets/search-sm.png';
import styled from 'styled-components';

export default function Search() {
  const prefixNode = BtnX => {
    return <DivIconEye imageIconX={BtnX}></DivIconEye>;
  };
  return (
    <ContainerSearch>
      <Input
        prefix={prefixNode(LogoI)}
        style={{
          maxWidth: '518px',
          width: '100%',
          height: '44px',
          background: '#EAEAEA',
          borderRadius: '29px',
          backgroundColor: '#EAEAEA',
        }}
        className="input_color"
      ></Input>
    </ContainerSearch>
  );
}

const ContainerSearch = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const DivIconEye = styled.div<{ imageIconX: string }>`
  width: 24px;
  height: 24px;
  background: url(${props => props.imageIconX}) no-repeat;
`;

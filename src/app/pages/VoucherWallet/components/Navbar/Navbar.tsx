import React from 'react';
import { Layout, Menu, Avatar } from 'antd';
import '../../VoucherWallet.less';
import styled from 'styled-components';
import Icons from '../../assets/icon.svg';
import { UserOutlined } from '@ant-design/icons';

export default function Navbar() {
  return (
    <Layout>
      <ContainerAvatar>
        <Avatar size={64} icon={<UserOutlined />} />
        <TagDiv>Anh Thư</TagDiv>
      </ContainerAvatar>
      <br />
      <Menu
        theme="light"
        mode="inline"
        defaultSelectedKeys={['1']}
        items={[
          {
            key: '1',
            icon: <DivIconEye imageIconX={Icons} />,
            label: 'Hồ sơ của tôi ',
            className: 'customeItem',
          },
          {
            key: '2',
            icon: <DivIconEye imageIconX={Icons} />,
            label: 'Hồ sơ của tôi ',
            className: 'customeItem',
          },
          {
            key: '3',
            icon: <DivIconEye imageIconX={Icons} />,
            label: 'Hồ sơ của tôi ',
            className: 'customeItem',
          },
        ]}
      />
    </Layout>
  );
}

export const DivIconEye = styled.div<{ imageIconX: string }>`
  width: 32px;
  height: 32px;
  background: url(${props => props.imageIconX}) no-repeat;
`;

const ContainerAvatar = styled.div`
  display: flex;
  align-items: center;
`;

const TagDiv = styled.h2`
  width: 158px;
  height: 31px;
  /* background-color: blue; */
`;

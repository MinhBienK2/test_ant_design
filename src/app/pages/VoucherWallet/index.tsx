import React from 'react';
import { Layout, Menu, Tabs, Row, Col } from 'antd';
import HeaderC from './../../components/HeaderC';
import './VoucherWallet.less';
import styled from 'styled-components';
import Icons from './assets/icon.svg';

import Navbar from './components/Navbar/Navbar';
import ContentSelect from './components/ContentSelecct';
const { Header, Footer, Sider, Content } = Layout;

export default function index() {
  return (
    <Layout>
      <HeaderC></HeaderC>
      <br />
      <br />
      <Layout
        style={{
          maxWidth: '1440px',
          width: '100%',
          margin: '0 auto',
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <Layout
          style={{
            maxWidth: '1170px',
            width: '100%',
            margin: '24px 16px 0',
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <Row justify={'space-between'} md-gutter={32}>
            <Col flex={'23.7606837607%'} xs={0} lg={6}>
              <Navbar></Navbar>
            </Col>
            <Col flex={'auto'}>
              <Content
                style={{
                  maxWidth: '860px',
                  width: '100%',
                  background: '#FFFFFF',
                  float: 'right',
                }}
              >
                <ContentSelect></ContentSelect>
              </Content>
            </Col>
          </Row>
        </Layout>
      </Layout>
    </Layout>
  );
}

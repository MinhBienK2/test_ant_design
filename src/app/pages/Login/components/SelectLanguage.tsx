import React from 'react';
import { Button, Popover } from 'antd';

const content = (
  <div>
    <p>VN</p>
    <p>EN</p>
  </div>
);

export default function SelectLanguage() {
  return (
    <Popover placement="bottom" content={content} trigger="click">
      <Button>Bottom</Button>
    </Popover>
  );
}

import { Layout, Row, Col, Divider, Form, Input, Checkbox, Button } from 'antd';
import React from 'react';

import './Login.less';

import Eye from './assets/icon.svg';
import styled from 'styled-components';

import Hideeye from './assets/hideicon.svg';

import Facebook from './assets/facebook2.png';
import Google from './assets/google1.png';
import Apple from './assets/pple1.png';
import LogoImage from './assets/logo.png';
import SelectLanguage from './components/SelectLanguage';
const { Header, Content, Footer, Sider } = Layout;

const App: React.FC = () => {
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  const suffixNode = BtnX => {
    return <DivIconEye imageIconX={BtnX}></DivIconEye>;
  };

  return (
    <Layout style={{ maxWidth: '1440px', width: '100%', margin: '0 auto' }}>
      <Content style={{ margin: '24px 16px 0' }}>
        <Row justify={'center'}>
          <Col
            style={{
              width: '100%',
              maxWidth: '1038px',
              display: 'flex',
              justifyContent: 'right',
            }}
          >
            <SelectLanguage></SelectLanguage>
          </Col>
        </Row>
        <Row>
          <Col
            style={{
              width: '100%',
              display: 'flex',
              justifyContent: 'center',
            }}
          >
            <Logo image={LogoImage}></Logo>
          </Col>
        </Row>
        <Row justify={'center'}>
          <Col
            xs={24}
            sm={24}
            md={24}
            lg={24}
            xl={24}
            style={{ maxWidth: '570px', width: '100%' }}
          >
            <Form
              name="basic"
              // labelCol={{ span: 8 }}
              // wrapperCol={{ span: 16 }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
              layout="vertical"
              className="formnestCssWithLess"
            >
              <Form.Item
                label="Username"
                name="username"
                rules={[
                  { required: true, message: 'Please input your username!' },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Password"
                name="password"
                rules={[
                  { required: true, message: 'Please input your password!' },
                ]}
              >
                <Input.Password
                  iconRender={visible =>
                    visible ? (
                      <DivIconEye imageIconX={Eye}></DivIconEye>
                    ) : (
                      <DivIconEye imageIconX={Hideeye}></DivIconEye>
                    )
                  }
                />
              </Form.Item>

              <Form.Item name="remember" valuePropName="checked">
                <Checkbox>Remember me</Checkbox>
              </Form.Item>

              <Form.Item className="btnLess_login">
                <Button
                  type="primary"
                  htmlType="submit"
                  className="lessBtnLogin"
                >
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
        <Row justify={'center'}>
          <Col style={{ maxWidth: '570px', width: '100%' }}>
            <Divider plain>Text</Divider>
          </Col>
        </Row>
        <Row justify={'center'}>
          <Col
            style={{
              maxWidth: '570px',
              width: '100%',
              display: 'flex',
              justifyContent: 'center',
            }}
          >
            <DivBoxOAuth>
              <IconImage
                className="hover-point"
                imageIcon={Facebook}
              ></IconImage>
              <IconImage className="hover-point" imageIcon={Google}></IconImage>
              <IconImage className="hover-point" imageIcon={Apple}></IconImage>
            </DivBoxOAuth>
          </Col>
        </Row>
      </Content>
    </Layout>
  );
};

export default App;

export const DivIconEye = styled.div<{ imageIconX: string }>`
  width: 20px;
  height: 20px;
  background: url(${props => props.imageIconX}) no-repeat;
`;

export const DivBoxOAuth = styled.div`
  display: flex;
  width: 320px;
  justify-content: space-between;
  @media screen and (max-width: 500px) {
    width: 240px;
  }
`;
export const IconImage = styled.div<{ imageIcon: string }>`
  width: 64px;
  height: 64px;
  background: url(${props => props.imageIcon});
  margin: 24px 0;
`;

export const Logo = styled.div<{ image: string }>`
  width: 100px;
  height: 100px;
  background: url(${props => props.image}) no-repeat;
  margin-bottom: 40px;
`;

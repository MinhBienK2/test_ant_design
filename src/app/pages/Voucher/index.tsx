import React from 'react';
import { Layout } from 'antd';
import HeaderC from './../../components/HeaderC';
import './Voucher.less';
import Slick from './components/Slick';
import GiveYou from './components/GiveYou';
const { Header, Footer, Sider, Content } = Layout;

export default function index() {
  return (
    <>
      <HeaderC></HeaderC>
      <Layout
        style={{
          maxWidth: '1440px',
          width: '100%',
          margin: '0 auto',
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <Content
          style={{ maxWidth: '1170px', width: '100%', margin: '24px 16px 0' }}
        >
          <Slick></Slick>
          <br></br>
          <Slick></Slick>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <GiveYou></GiveYou>
        </Content>
      </Layout>
    </>
  );
}

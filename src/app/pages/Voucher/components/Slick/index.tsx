import { Carousel } from 'antd';
import React from 'react';

const contentStyle: React.CSSProperties = {
  margin: '0 5px',
  height: '160px',
  color: '#fff',
  lineHeight: '160px',
  textAlign: 'center',
  background: '#364d79',
};

const option = {
  slidesToShow: 9,
  infinite: true,
  arrows: true,
  centerPadding: '50px',
  dots: false,
  swipeToSlide: true,
  swipe: true,
  draggable: true,
  // accessibility: true,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 8,
      },
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 6,
      },
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 5,
      },
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 4,
      },
    },
    {
      breakpoint: 460,
      settings: {
        slidesToShow: 3,
      },
    },
  ],
};

export default function index() {
  return (
    <>
      <h3>Thương hiệu nổi bật</h3>
      <Carousel {...option}>
        <div>
          <h3 style={contentStyle}>1</h3>
        </div>
        <div>
          <h3 style={contentStyle}>2</h3>
        </div>
        <div>
          <h3 style={contentStyle}>3</h3>
        </div>
        <div>
          <h3 style={contentStyle}>4</h3>
        </div>
        <div>
          <h3 style={contentStyle}>5</h3>
        </div>
        <div>
          <h3 style={contentStyle}>6</h3>
        </div>
        <div>
          <h3 style={contentStyle}>7</h3>
        </div>
        <div>
          <h3 style={contentStyle}>8</h3>
        </div>
        <div>
          <h3 style={contentStyle}>9</h3>
        </div>
        <div>
          <h3 style={contentStyle}>10</h3>
        </div>
        <div>
          <h3 style={contentStyle}>11</h3>
        </div>
        <div>
          <h3 style={contentStyle}>4</h3>
        </div>
        <div>
          <h3 style={contentStyle}>4</h3>
        </div>
        <div>
          <h3 style={contentStyle}>4</h3>
        </div>
      </Carousel>
    </>
  );
}

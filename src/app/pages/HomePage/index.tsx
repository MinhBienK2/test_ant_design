import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { Button } from 'antd';
import './HomePage.less';

export function HomePage() {
  return (
    <>
      <Helmet>
        <title>HomePage</title>
        <meta name="description" content="A Boilerplate application homepage" />
      </Helmet>
      <Button type="primary">Button</Button>
    </>
  );
}

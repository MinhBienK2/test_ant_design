import React from 'react';
import { Divider } from 'antd';
import LayoutVoucher from '../../../layout/StoreAndVoucher';
import Information from './components/information';

export default function index() {
  return (
    <LayoutVoucher>
      <Information></Information>
    </LayoutVoucher>
  );
}

import React from 'react';
import styled from 'styled-components';

export default function index() {
  return (
    <Container>
      <Box style={{ height: '115px', background: 'red' }}>
        <h2>Nội dung voucher</h2>
        <p>
          E-voucher giá chỉ 129k cho 2 vé xem phim và 1 bỏng khi thanh toán bằng
          mã Voucher hoặc Scan mã QR tại tất cả các cửa hàng trong danh sách Địa
          điểm sử dụng.
        </p>
      </Box>
      <br />
      <Box style={{ height: '115px', background: 'red' }}>
        <h2>Nội dung voucher</h2>
        <p>
          E-voucher giá chỉ 129k cho 2 vé xem phim và 1 bỏng khi thanh toán bằng
          mã Voucher hoặc Scan mã QR tại tất cả các cửa hàng trong danh sách Địa
          điểm sử dụng.
        </p>
      </Box>
      <br />
      <Box style={{ height: '115px', background: 'red' }}>
        <h2>Nội dung voucher</h2>
        <p>
          E-voucher giá chỉ 129k cho 2 vé xem phim và 1 bỏng khi thanh toán bằng
          mã Voucher hoặc Scan mã QR tại tất cả các cửa hàng trong danh sách Địa
          điểm sử dụng.
        </p>
      </Box>
      <br />
    </Container>
  );
}

const Container = styled.div`
  width: 100%;
  max-width: 1030px;
  margin: 0 auto;
`;

const Box = styled.div`
  padding: 0 30px;
`;

/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import { GlobalStyle } from 'styles/global-styles';

import { HomePage } from './pages/HomePage/Loadable';
import { NotFoundPage } from './components/NotFoundPage/Loadable';
import Login from 'app/pages/Login';
import Voucher from 'app/pages/Voucher';
import VoucherWallet from 'app/pages/VoucherWallet';
import VoucherDetail from 'app/pages/VoucherDetail';
import VoucherImage from 'app/pages/VoucherImage';

import { useTranslation } from 'react-i18next';

export function App() {
  const { i18n } = useTranslation();
  return (
    <BrowserRouter>
      <Helmet
        titleTemplate="%s - React Boilerplate"
        defaultTitle="React Boilerplate"
        htmlAttributes={{ lang: i18n.language }}
      >
        <meta name="description" content="A React Boilerplate application" />
      </Helmet>

      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/login" element={<Login />} />
        <Route path="/voucher" element={<Voucher />} />
        <Route path="/voucher-wallet" element={<VoucherWallet />} />
        <Route path="/voucher-detail" element={<VoucherDetail />} />
        <Route path="/voucher-image" element={<VoucherImage />} />
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
      <GlobalStyle />
    </BrowserRouter>
  );
}

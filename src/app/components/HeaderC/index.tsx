import React from 'react';
import { Layout, Row, Col } from 'antd';
import './Header.less';
import styled from 'styled-components';
import './Header.less';

import InputSearch from './components/inputSearch';
import Menu from './components/Menu';
import AvatarC from './components/AvatarC';

const { Header, Footer, Sider, Content } = Layout;

export default function index() {
  return (
    <Layout
      className="layou_search"
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Header className="header-less">
        <Row justify={'center'}>
          <Col lg={12} md={10}>
            <InputSearch></InputSearch>
          </Col>
          <Col
            lg={12}
            md={14}
            style={{ display: 'flex', justifyContent: 'center' }}
          >
            <GroupTwo>
              <Menu></Menu>
              <AvatarC></AvatarC>
            </GroupTwo>
          </Col>
        </Row>
      </Header>
    </Layout>
  );
}

const GroupTwo = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  width: 100%;
`;

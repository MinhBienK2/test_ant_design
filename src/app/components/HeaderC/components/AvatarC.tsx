import React from 'react';
import styled from 'styled-components';

import { UserOutlined } from '@ant-design/icons';
import { Avatar, Col, Row } from 'antd';

export default function AvatarC() {
  return (
    <ContainerAvatar>
      <TagDiv></TagDiv>
      <TagDiv></TagDiv>
      <Avatar size={64} icon={<UserOutlined />} />
    </ContainerAvatar>
  );
}

const ContainerAvatar = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const TagDiv = styled.div`
  max-width: 77px;
  width: 77px;
  height: 31px;
  background-color: blue;
`;

import React from 'react';
import styled from 'styled-components';
import { Button, Input, Space } from 'antd';

import LogoI from '../assets/Asset 9 5.png';
import Search from '../assets/search-sm.png';

export default function inputSearch() {
  const prefixNode = BtnX => {
    return <DivIconEye imageIconX={BtnX}></DivIconEye>;
  };

  return (
    <ContainerSearch>
      <Logo image={LogoI}></Logo>
      <Input
        prefix={prefixNode(Search)}
        style={{
          maxWidth: '518px',
          width: '100%',
          height: '44px',
          background: '#EAEAEA',
          borderRadius: '29px',
          backgroundColor: '#EAEAEA',
        }}
        className="input_color"
      ></Input>
      <Button>Đăng bán</Button>
    </ContainerSearch>
  );
}

const ContainerSearch = styled.div`
  display: flex;
  align-items: center;
`;

const Logo = styled.div<{ image: string }>`
  width: 100px;
  height: 100px;
  min-width: 100px;
  background: url(${props => props.image});
  /* margin-right: 5px; */
`;

export const DivIconEye = styled.div<{ imageIconX: string }>`
  width: 24px;
  height: 24px;
  background: url(${props => props.imageIconX}) no-repeat;
`;

import React from 'react';
import styled from 'styled-components';

import Icons from '../assets/Frame 598.png';

export default function Menu() {
  return (
    <ContainerMenu>
      <Icon image={Icons}></Icon>
      <Icon image={Icons}></Icon>
      <Icon image={Icons}></Icon>
      <Icon image={Icons}></Icon>
    </ContainerMenu>
  );
}

const ContainerMenu = styled.div`
  display: flex;
  /* justify-content: space-evenly; */
`;

const Icon = styled.div<{ image: string }>`
  width: 64px;
  height: 40px;
  background: url(${props => props.image});
`;
